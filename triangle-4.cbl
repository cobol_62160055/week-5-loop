       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-4.
       AUTHOR. Chenpob.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE    PIC X(80) VALUE SPACE .
       01  STAR-NUM    PIC 9(3)  VALUE ZEROES .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01  INDEX-NUM1   PIC 9(3)  VALUE ZEROES .
       01  INDEX-NUM2   PIC 9(3)  VALUE ZEROES .

       PROCEDURE DIVISION .
           PERFORM 001-INPUT-STAR-NUM THRU 001-EXIT 
           PERFORM 002-DISPLAY-STAR-LINE THRU 002-EXIT 
             VARYING INDEX-NUM1 FROM 1 BY 1
             UNTIL INDEX-NUM1 > STAR-NUM 
           GOBACK 
       .

       001-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-NUM > 0 AND STAR-NUM <= 80
            DISPLAY "Please input star number: " WITH NO ADVANCING 
            ACCEPT STAR-NUM 
            IF NOT VALID-STAR-NUM DISPLAY "Please input star number in p
      -     "ositive number"
            END-IF 
           END-PERFORM
       .
       001-EXIT.
           EXIT
       .
       002-DISPLAY-STAR-LINE.
           COMPUTE INDEX-NUM2 = STAR-NUM - INDEX-NUM1 + 1
           MOVE ALL SPACES TO SCR-LINE 
           MOVE ALL "*" TO SCR-LINE (INDEX-NUM1:INDEX-NUM2)
           DISPLAY SCR-LINE 
       .

       002-EXIT.
           EXIT 
       .

