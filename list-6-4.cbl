       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING6-3.
       AUTHOR. Chenpob.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  COUNTERS.
           05 HUNDREDS-COUNT  PIC 99 VALUE ZEROES .
           05 TEN-COUNT  PIC 99  VALUE ZEROES .
           05 UNITS-COUNT PIC 99 VALUE ZEROES .
       01  ODOMETER.
           05 PRN-HUNDREDS   PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-TENS       PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-UNITS      PIC 9.
       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "Using an out-of-line perform"
      *    out-line 
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
             VARYING HUNDREDS-COUNT FROM 0 BY 1 
              UNTIL HUNDREDS-COUNT > 9
             AFTER TEN-COUNT FROM 0 BY 1 UNTIL TEN-COUNT > 9
             AFTER UNITS-COUNT FROM 0 BY 1 UNTIL UNITS-COUNT > 9
      *    in-line
      *    PERFORM  VARYING HUNDREDS-COUNT FROM 0 BY 1 
      *       UNTIL HUNDREDS-COUNT > 9
      *       PERFORM VARYING TEN-COUNT FROM 0 BY 1 
      *       UNTIL TEN-COUNT > 9
      *       PERFORM VARYING UNITS-COUNT FROM 0 BY 1 
      *       UNTIL UNITS-COUNT > 9
      *         MOVE HUNDREDS-COUNT TO PRN-HUNDREDS 
      *         MOVE TEN-COUNT TO PRN-TENS 
      *         MOVE UNITS-COUNT TO PRN-UNITS 
      *         DISPLAY "Out - " ODOMETER 
      *    END-PERFORM
      *    END-PERFORM
      *    END-PERFORM
           GOBACK 
       .
       001-COUNT-MILEAGE.
           MOVE HUNDREDS-COUNT TO PRN-HUNDREDS 
           MOVE TEN-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNITS 
           DISPLAY "Out - " ODOMETER 
       .
       001-EXIT.
           EXIT 
       .